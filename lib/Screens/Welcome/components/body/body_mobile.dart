import 'package:flutter/material.dart';
import 'package:flutter_auth/Screens/Login/login_screen.dart';
import 'package:flutter_auth/Screens/Signup/signup_screen.dart';
import 'package:flutter_auth/Screens/Welcome/components/background.dart';
import 'package:flutter_auth/components/rounded_button.dart';
// import 'package:flutter_auth/constants.dart';
// import 'package:flutter_svg/svg.dart';

class BodyMobile extends StatelessWidget {
  const BodyMobile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // This size provide us total height and width of our screen
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // const Text(
            //   "Bienvenidos a AMOL",
            //   style: TextStyle(
            //       fontWeight: FontWeight.bold,
            //       color: kPrimaryColor,
            //       fontSize: 30),
            // ),
            SizedBox(height: size.height * 0.04),
            Image.asset(
              "assets/images/LOGO COLABORAMED 4.png",
              height: size.height * 0.15,
            ),
            Image.asset(
              "assets/images/Tarjeta-Smartphone.5277675.png",
              height: size.height * 0.25,
            ),
            SizedBox(height: size.height * 0.04),
            RoundedButton(
              text: "Ingresar",
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return const LoginScreen();
                    },
                  ),
                );
              },
            ),
            RoundedButton(
              text: "Registrarse",
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return const SignUpScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
