import 'package:flutter/material.dart';
import 'components/body/body.dart';
import 'package:flutter_auth/components/nav_bar.dart';

class DashbordScreen extends StatelessWidget {
  const DashbordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Colabor@med'),
          backgroundColor: Color.fromARGB(255, 217, 62, 85)),
      drawer: NavBar(),
      body: Body(),
    );
  }
}
