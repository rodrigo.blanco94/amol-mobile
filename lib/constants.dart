import 'package:flutter/material.dart';

const kPrimaryColor = Color.fromARGB(255, 217, 62, 85);
const kPrimaryLightColor = Color.fromARGB(255, 106, 136, 199);
const kSecundaryColor = Color.fromARGB(255, 33, 6, 6);
const bMenuButton = Color.fromARGB(255, 87, 146, 204);
